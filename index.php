<?php

if(empty($_GET)) {
?><html><body><?php
 
 foreach(glob('*', GLOB_ONLYDIR) as $dir) {
  echo $dir;?><br>
   <?php echo "http://".$_SERVER['HTTP_HOST']."/?q=".rawurlencode($dir); ?><br>
   <img src="qrlink.php?q=<?php echo rawurlencode($dir);?>">
  <br><br><br><?php
 }

?></body></html><?php

} elseif(strpos(realpath($_GET["q"]), getcwd()) !== false) {

 header('Content-type: audio/x-mpegurl');
 header('Content-Disposition: inline; filename="index.vlc"');
 
 echo "#EXTM3U\n\n";
 
 foreach(getDirContents(realpath($_GET["q"])) as $file) {
  
  ob_start();
  passthru("ffprobe -v quiet -print_format json -show_format ".escapeshellarg($file));
  $meta = json_decode(ob_get_clean());
  
  $id = preg_replace("/[^[:alnum:][:space:]]/u", ' ', $file);
  $id = str_replace(array(
    "avi",
    "season"
  ), ' ', $id);
  $id = preg_replace('/\s+/', ' ', $id);
  $id = substr($id, -30);
 
  $file = rawurlencode(str_replace(getcwd()."/", "", $file)); //remove absolute path and encode
  $file = str_replace("%2F", "/", $file); //unencode folder separators
  $url = "http://".$_SERVER['HTTP_HOST']."/".$file; //format as link
  
  //m3u format
  echo "#EXTINF:".round($meta->format->duration).", x - ".$id."\n";
  echo $url."\n\n";
 }

}

function getDirContents($dir, &$results = array()) {
    $files = scandir($dir);

    foreach ($files as $key => $value) {
        $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
        if (!is_dir($path)) {
            $results[] = $path;
        } else if ($value != "." && $value != "..") {
            getDirContents($path, $results);
            $results[] = $path;
        }
    }

    return $results;
}
