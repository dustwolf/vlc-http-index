# vlc-http-index

This is a simple PHP tool that lists folder indexes of videos in a way that can be played on mobile conveniently.

It assumes that each immediate folder is a separate playlist of videos within. 

Each playlist is displayed as a QR code, which you can scan and open in mobile VLC, then all the videos within that folder will be listed in your playlist.

## credit

This project uses PHPQRCode: http://phpqrcode.sourceforge.net/

It was quickly thrown together based on a number of Stackoverflow posts.

## install

Configure your webserver to serve everything from your videos folder. Limit access by IP for security.

Place these files in the root folder.

## usage

Open the main index on a PC. Scan the QR code of the directory you wish to play and open it in VLC.

VLC will have access to the entire playlist, so you can choose and seek as needed within VLC.

The idea is to use this within your local network.
